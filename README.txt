- To install all plugins run
    git-clone.csh

- To update all plugins run
    git-pull.csh

- To install powerline/airline fonts
    mkdir ~/.fonts
    cd ~/.fonts
    git clone git://github.com/Lokaltog/powerline-fonts.git


